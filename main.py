from math import sqrt
from datetime import timedelta, datetime
from typing import Tuple


def atkin_sieve(given_number: int) -> Tuple[list, timedelta]:
    """Implementation of the Sieve of Atkin algorithm for finding prime numbers up to the given number,
    Asymptotic time complexity O(n).
    :param given_number: The number we want to generate prime numbers up to.
    :return: Found prime numbers with total execution time.
    """
    start = datetime.now()
    elapsed_time = timedelta(0)
    res = []
    sieve_filter1 = {1, 13, 17, 29, 37, 41, 49, 53}
    sieve_filter2 = {7, 19, 31, 43}
    sieve_filter3 = {11, 23, 47, 59}

    # Initializing main list indicating prime numbers
    print("Generating the main list indicator")
    sieve_list = [False for _ in range(given_number + 1)]

    # Since the main algorithm does not take care of these numbers, they need to be included manually.
    if given_number >= 2:
        res.append(2)
    if given_number >= 3:
        res.append(3)
    if given_number >= 5:
        res.append(5)

    print("The process of generating prime numbers has been started")
    for x in range(1, int(sqrt(given_number) + 1)):
        for y in range(1, int(sqrt(given_number) + 1)):

            n = (4 * x ** 2) + (y ** 2)
            if n <= given_number and n % 60 in sieve_filter1:
                sieve_list[n] ^= True

            n = (3 * x ** 2) + (y ** 2)
            if n <= given_number and n % 60 in sieve_filter2:
                sieve_list[n] ^= True

            n = (3 * x ** 2) - (y ** 2)
            if (x > y and n <= given_number and
                    n % 60 in sieve_filter3):
                sieve_list[n] ^= True

            elapsed_time = datetime.now() - start

    squares_filter(given_number, sieve_list)

    # Result composition
    res += [index for index, element in enumerate(sieve_list) if element]
    return res, elapsed_time


# Helper functions
def squares_filter(given_number: int, sieve: list) -> None:
    for i in range(1, int(sqrt(given_number) + 1)):
        if sieve[i]:
            for i in range(i ** 2, given_number + 1, i ** 2):
                sieve[i] = False


def print_results(result, execution_time):
    """Helper function to format and print results in a desired way
    :param result: List of prime numbers to be printed
    :param execution_time: Total execution time of the program.
    """
    if len(result) >= 20:
        print(f"First 10 prime numbers are --->>> {result[:10]}")
        print(f"Last 10 prime numbers are --->>> {result[-10:]}")
    else:
        print(f"Total amount of generated prime numbers less than 20 -->>> printing out all of them {result}")

    print(f"Total execution time --->>> {execution_time}")


if __name__ == "__main__":

    # Code execution
    valid = False
    print("Starting....")
    while not valid:
        try:
            given_number = int(input("Please provide the max number you want to compute prime numbers up to \n"))
            if given_number <= 0:
                raise ValueError("Number has to be greater than zero")
            valid = True
        except ValueError as e:
            print(f"You have provided incorrect value, please enter the positive integer -->>>> {e}")

    result, execution_time = atkin_sieve(given_number)
    print_results(result, execution_time)
