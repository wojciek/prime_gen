# Prime Gen

Implementation of an algorithm generating consecutive prime numbers up to given N number.

## Program description

The programme implementing sieve of Atkin algorithm. It is an optimized (in terms of time complexity)
sieve of Eratosthenes algorithm. In addition to what Eratosthenes does, which is marking off multiples
of squares of primes, it does some preliminary filtering. Each time when solution of some predefined
quadratic equation is met and the reminder of the division of given number n by 60 is one of the element
in the predefined set, the indicator for the number is being changed for the oppostite (Initially being False).

## Testing results

1 minute execution biggest prime number: 10999997
5 minutes execution biggest prime number: 54999869

## How to execute

To execute you will need a working version of python v3.6 (or higher) interpreter installed.
Script does not require any external packages to be installed.

Execute commands
```
python main.py
```
Optionally:
```
python3 main.py
```

